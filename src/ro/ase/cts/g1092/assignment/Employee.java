package ro.ase.cts.g1092.assignment;

public class Employee implements Feedbackable
{
	public String empUsername;
	public String empMail;
	public boolean isHappy;
	public int no;
	
	public Employee(String empUsername, String empMail, int no) 
	{
		super();
		this.empUsername = empUsername;
		this.empMail= empMail;
		this.no = no;
	}

	@Override
	public void getFeedback(boolean isHappy, int no)
	{
		if(no == 0)
		{
			isHappy = false;
			System.out.println("We appreciate your feedback. P.S.: Why so sad today?");
		}
		else
		{
			isHappy = true;
			System.out.println("We appreciate your feedback. P.S.: Someone is feeling good today! ");
		}		
	}
}
