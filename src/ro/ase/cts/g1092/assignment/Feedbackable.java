package ro.ase.cts.g1092.assignment;

public interface Feedbackable 
{
	public void getFeedback(boolean isHappy, int no);
}
